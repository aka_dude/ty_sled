#![feature(generic_associated_types)]

#[cfg(test)]
#[test]
fn test1() {
    use serde::{Deserialize, Serialize};
    use ty_sled::{ty_table, TyDb, TyTable};

    #[derive(Debug, Deserialize, Serialize)]
    struct Person<'a> {
        age: u8,
        name: &'a str,
    }

    ty_table! {
        MyTestTable {
            NAME => b"my_test_table",
            Key<'a> => u32,
            Value<'a> => Person<'a>
        }
    }

    let db = sled::Config::new()
        .temporary(true)
        .create_new(true)
        .open()
        .unwrap();

    let table: MyTestTable = db.open_table().unwrap();

    let first = Person {
        age: 20,
        name: "Lol Kek",
    };
    assert!(matches!(table.insert_ignore(&1, &first), Ok(false)));

    let second = Person {
        age: 27,
        name: "Lol Kek Cheburek",
    };
    assert!(matches!(table.insert_ignore(&2, &second), Ok(false)));

    let mut iter = table.iter();
    while let Some(Ok((key, val))) = iter.next_item() {
        println!("{} -> {:?}", key, val);
    }
}
