#![feature(generic_associated_types, try_blocks)]

use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::marker::PhantomData;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("sled error: {0}")]
    Sled(#[from] sled::Error),
    #[error("bincode error: {0}")]
    Bincode(#[from] bincode::Error),
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

/// Typed table, a wrapper for [`sled::Tree`](sled::Tree).
/// All methods are just strongly-types versions of `sled::Tree`'s methods.
/// Intended to be implemented by [`ty_table!`](ty_table).
pub trait TyTable: From<sled::Tree> + AsRef<sled::Tree> {
    /// Table's name, passed to [`sled::Db::open_tree`](sled::Db::open_tree).
    const NAME: &'static [u8];
    /// Key type. It's always behind reference, thus may be `!Sized`.
    type Key<'a>: Deserialize<'a> + Serialize;

    /// Value type. May be [`!DeserializeOwned`](serde::de::DeserializeOwned),
    /// but in this case [`TyTableOwned`](TyTableOwned) won't be implemented for table.
    type Value<'a>: Deserialize<'a> + Serialize;

    fn get<'a>(
        &self,
        key: &Self::Key<'_>,
        buffer: &'a mut sled::IVec,
    ) -> Result<Option<Self::Value<'a>>> {
        self.as_ref()
            .get(&bincode::serialize(key)?)
            .map_err(Error::from)
            .and_then(move |opt| {
                opt.map(move |val| {
                    *buffer = val;
                    bincode::deserialize(buffer).map_err(Error::from)
                })
                .transpose()
            })
    }

    fn insert<'a>(
        &self,
        key: &Self::Key<'_>,
        value: &Self::Value<'_>,
        buffer: &'a mut sled::IVec,
    ) -> Result<Option<Self::Value<'a>>> {
        self.as_ref()
            .insert(&bincode::serialize(key)?, bincode::serialize(value)?)
            .map_err(Error::from)
            .and_then(move |opt| {
                opt.map(move |val| {
                    *buffer = val;
                    bincode::deserialize(buffer).map_err(Error::from)
                })
                .transpose()
            })
    }

    fn insert_ignore(&self, key: &Self::Key<'_>, value: &Self::Value<'_>) -> Result<bool> {
        self.as_ref()
            .insert(&bincode::serialize(key)?, bincode::serialize(value)?)
            .map_err(Error::from)
            .map(|opt| opt.is_some())
    }

    fn remove<'a>(
        &self,
        key: &Self::Key<'_>,
        buffer: &'a mut sled::IVec,
    ) -> Result<Option<Self::Value<'a>>> {
        self.as_ref()
            .remove(&bincode::serialize(key)?)
            .map_err(Error::from)
            .and_then(move |opt| {
                opt.map(move |val| {
                    *buffer = val;
                    bincode::deserialize(buffer).map_err(Error::from)
                })
                .transpose()
            })
    }

    fn iter(&self) -> TyIter<Self> {
        TyIter::from(self.as_ref().iter())
    }
}

/// Owned extension for typed table [`TyTable`](TyTable).
/// Automatically implemented for tables, whose `Value` implements [`serde::de::DeserializeOwned`](serde::de::DeserializeOwned).
pub trait TyTableOwned: TyTable
where
    for<'a> Self::Value<'a>: DeserializeOwned,
{
    fn get_owned(&self, key: &Self::Key<'_>) -> Result<Option<Self::Value<'_>>> {
        self.as_ref()
            .get(&bincode::serialize(key)?)
            .map_err(Error::from)
            .and_then(move |opt| {
                opt.map(move |val| bincode::deserialize(&val).map_err(Error::from))
                    .transpose()
            })
    }

    fn insert_owned(
        &self,
        key: &Self::Key<'_>,
        value: &Self::Value<'_>,
    ) -> Result<Option<Self::Value<'_>>> {
        self.as_ref()
            .insert(&bincode::serialize(key)?, bincode::serialize(value)?)
            .map_err(Error::from)
            .and_then(move |opt| {
                opt.map(move |val| bincode::deserialize(&val).map_err(Error::from))
                    .transpose()
            })
    }
}

impl<T> TyTableOwned for T
where
    T: TyTable,
    for<'a> T::Value<'a>: DeserializeOwned,
{
}

pub struct TyIter<T>
where
    T: TyTable,
{
    inner: sled::Iter,
    key_buf: sled::IVec,
    val_buf: sled::IVec,
    parent: PhantomData<T>,
}

impl<T> TyIter<T>
where
    T: TyTable,
{
    pub fn next_item(&mut self) -> Option<Result<(T::Key<'_>, T::Value<'_>)>> {
        let (key_buf, val_buf) = match self.inner.next()? {
            Ok(pair) => pair,
            Err(err) => return Some(Err(err.into())),
        };
        self.key_buf = key_buf;
        self.val_buf = val_buf;
        Some(
            try {
                (
                    bincode::deserialize(self.key_buf.as_ref())?,
                    bincode::deserialize(self.val_buf.as_ref())?,
                )
            },
        )
    }
}

impl<T> From<sled::Iter> for TyIter<T>
where
    T: TyTable,
{
    fn from(inner: sled::Iter) -> Self {
        TyIter {
            inner,
            key_buf: sled::IVec::default(),
            val_buf: sled::IVec::default(),
            parent: PhantomData::default(),
        }
    }
}

#[macro_export]
/// Convenient macro for creating newtype wrappers around [`sled::Tree`](sled::Tree).
///
/// # Example
///
/// ```
/// #![feature(generic_associated_types)]
///
/// use serde::{Deserialize, Serialize};
/// use ty_sled::ty_table;
///
/// #[derive(Debug, Deserialize, Serialize)]
/// struct Person<'a> {
///     age: u8,
///     name: &'a str,
/// }
///
/// ty_table! {
///     MyTestTable {
///         NAME => b"my_test_table",
///         Key<'a> => u32,
///         Value<'a> => Person<'a>
///     }
/// }
/// ```
///
/// This code emits something like the following:
///
/// ```
/// #![feature(generic_associated_types)]
///
/// use serde::{Deserialize, Serialize};
/// use ty_sled::ty_table;
///
/// #[derive(Debug, Deserialize, Serialize)]
/// struct Person<'a> {
///     age: u8,
///     name: &'a str,
/// }
///
/// #[derive(Clone, Debug)]
/// struct MyTestTable(sled::Tree);
///
/// impl ty_sled::TyTable for MyTestTable {
///     const NAME: &'static [u8] = b"my_test_table";
///     type Key<'a> = u32;
///     type Value<'a> = Person<'a>;
/// }
///
/// impl From<sled::Tree> for MyTestTable {
///     fn from(val: sled::Tree) -> Self {
///         MyTestTable(val)
///     }
/// }
///
/// impl AsRef<sled::Tree> for MyTestTable {
///     fn as_ref(&self) -> &sled::Tree {
///         &self.0
///     }
/// }
/// ```
///
/// Very simple. All useful methods are in [`TyTable`](TyTable).
macro_rules! ty_table {
    ($table:ident {
        NAME => $name:expr,
        Key $(<'a>)? => $key:ty,
        Value $(<'a>)? => $value:ty $(,)?
    }) => {
        #[derive(::std::clone::Clone, ::std::fmt::Debug)]
        struct $table(::sled::Tree);

        impl ::ty_sled::TyTable for $table {
            const NAME: &'static [u8] = $name;
            type Key<'a> = $key;
            type Value<'a> = $value;
        }

        impl ::std::convert::From<::sled::Tree> for $table {
            fn from(val: ::sled::Tree) -> Self {
                $table(val)
            }
        }

        impl ::std::convert::AsRef<::sled::Tree> for $table {
            fn as_ref(&self) -> &::sled::Tree {
                &self.0
            }
        }
    };
}

/// Typed extension for [`sled::Db`](sled::Db).
pub trait TyDb {
    fn open_table<T: TyTable>(&self) -> sled::Result<T>;
}

impl TyDb for sled::Db {
    fn open_table<T: TyTable>(&self) -> sled::Result<T> {
        self.open_tree(T::NAME).map(T::from)
    }
}
